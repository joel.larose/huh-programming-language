﻿grammar huh;

tokens {
/* Not sure yet whether all of these will be featured in the language, but for now, let's reserve them. */
    KW_TRUE,
    KW_FALSE,
    KW_NONE,
    KW_AND,
    KW_AS,
    KW_ASSERT,
    KW_ASYNC,
    KW_AWAIT,
    KW_BREAK,
    KW_CLASS,
    KW_CONTINUE,
    KW_DEF,
    KW_DEL,
    KW_ELIF,
    KW_ELSE,
    KW_EXCEPT,
    KW_FINALLY,
    KW_FOR,
    KW_FROM,
    KW_GLOBAL,
    KW_IF,
    KW_IMPORT,
    KW_IN,
    KW_IS,
    KW_LAMBDA,
    KW_NONLOCAL,
    KW_NOT,
    KW_OR,
    KW_PASS,
    KW_RAISE,
    KW_RETURN,
    KW_TRY,
    KW_WHILE,
    KW_WITH,
    KW_YIELD,
    KW_PACKAGE,
    KW_PUBLIC,
    KW_PRIVATE,
    KW_INHERITED,
    KW_EXTENDS,
    KW_IMPLEMENTS,
    KW_STATIC,
    KW_FINAL,
    KW_SUPER,
    INDENT,
    DEDENT
}

/* Anywhere a space can be, so can a block comment.  For now insist on some space around block comments where this rule applies */
s : SPACE (block_comment SPACE)*;

module :
    kw_lang EOL
    (   SPACE
        | EOL
        | comment
        | statement
    )* EOF
    ;

kw_lang : ';' LANG (s LANG)* s?;

comment
    : block_comment
//    | line_comment
    ;

block_comment
    : '⌜' (comment|.)*? '⌟'  /* U+231C TOP LEFT CORNER, U+231F BOTTOM RIGHT CORNER */
    | '⌝' (comment|.)*? '⌞'  /* U+231C TOP RIGHT CORNER, U+231C BOTTOM LEFT CORNER */
    ;
//line_comment
//    : '#' COMMENT_LINE_TEXT EOL
//    ;

statement
    : assignment
    | class_def
    | function_def
    | import_statement
    ;

assignment: assignable s? ASSIGN_OP s? expression;

assignable
    : ID
    ;

expression
    : expression s? ('*' | '/') s? expression
    | expression s? ('+' | '-') s? expression
    | number_litteral
    | ID
    | STRING
    | '(' s? expression s? ')'
    ;

class_def: KW_CLASS s ID s [KW_EXTENDS s ID (s? ',' s? ID)*] s? ':' s? EOL (INDENT statement)+ ;

function_def: KW_FUNCTION s ID

number_litteral
    : INT_10
    | FLOAT_10
    | COMPLEX_IMAGE_10
    | INT_RADIX
    | FLOAT_RADIX
    | COMPLEX_IMAGE_RADIX
    ;

COMMENT_LINE_TEXT : ~[\r\n]+ ;

/* Syntax symbols */
ELLIPSIS
    : '...'  /* ASCII fallback */
    | '…'    /* U+2026 HORIZONTAL ELLIPSIS */
    ;
ASSIGN_OP
    : ':='  /* ASCII fallback */
    | '≔'  /* U+2254 COLON EQUALS -- Meant for left-to-right contexts */
    | '≕'  /* U+2255 EQUALS COLON -- Meant for right-to-left contexts */
    ;
SEQUENCE_OP: ',';
DOT : '.';
ASTERISK : '*';
OPEN_PAREN : '(' {self.opened += 1};
CLOSE_PAREN : ')' {self.opened -= 1};
COMMA : ',';
COLON : ':';
SEMI_COLON : ';';
POWER_OP : '**';
ROOT_OP : '√';  /* U+221A SQUARE ROOT : radical sign */
OPEN_BRACK : '[' {self.opened += 1};
CLOSE_BRACK : ']' {self.opened -= 1};
OR_OP
    : '|'  /* ASCII fallback 1 */
    | '\/' /* ASCII fallback 2 */
    | '∨'  /* U+2228 LOGICAL OR : vee, disjunction */
    ;
XOR_OP
    : '⊻'  /* U+22BB XOR */
    ;
AND_OP
    : '&'  /* ASCII fallback 1 */
    | '/\\' /* ASCII fallback 2 */
    | '∧'  /* U+2227 LOGICAL AND : wedge, conjunction */
    ;
LEFT_SHIFT
    : '<<' /* ASCII fallback */
    | '≪'  /* U+226A MUCH LESS-THAN */
    ;
RIGHT_SHIFT
    : '>>' /* ASCII fallback */
    | '≫'  /* U+226B MUCH GREATER-THAN */
    ;
ADD_OP : '+';
MINUS_OP : '-';
MULT_OP
    : '*'  /* Classic, ASCII fallback */
    | '×'  /* U+00D7 MULTIPLICATION SIGN : z notation Cartesian product */
    ;
DIV_OP
    : '/'  /* Classic, ASCII fallback */
    | '∕'  /* U+2215 DIVISION SLASH */
    | '÷'  /* U+00F7 DIVISION SIGN : obelus */
    ;

MOD_OP : '%';
IDIV_OP : '//';
NOT_OP
    : '~'
    | '¬'  /* U+00AC NOT SIGN : angled dash (in typography) */
    ;
OPEN_BRACE : '{' {self.opened += 1};
CLOSE_BRACE : '}' {self.opened -= 1};
LESS_THAN : '<';
GREATER_THAN : '>';
EQUALS
    : '?=' /* ASCII fallback */
    | '≟'  /* U+225F QUESTIONED EQUAL TO */
    ;
GT_EQ
    : '>=' /* ASCII fallback */
    | '≥'  /* U+2265 GREATER-THAN OR EQUAL TO */
    ;
LT_EQ
    : '<=' /* ASCII fallback */
    | '≤'  /* U+2264 LESS-THAN OR EQUAL TO */
    ;
NOT_EQ_1 : '<>';
NOT_EQ_2 : '!=';
AT : '@';
ARROW
    : '->' /* ASCII fallback */
    | '→'  /* U+2192 RIGHTWARDS ARROW : z notation total function */
    ;
fragment OP_ASSIGN: '=';
ADD_ASSIGN : ADD_OP OP_ASSIGN;
SUB_ASSIGN : MINUS_OP OP_ASSIGN;
MULT_ASSIGN : MULT_OP OP_ASSIGN;
AT_ASSIGN : AT OP_ASSIGN;
DIV_ASSIGN : DIV_OP OP_ASSIGN;
MOD_ASSIGN : MOD_OP OP_ASSIGN;
AND_ASSIGN : AND_OP OP_ASSIGN;
OR_ASSIGN : OR_OP OP_ASSIGN;
XOR_ASSIGN : XOR_OP OP_ASSIGN;
LEFT_SHIFT_ASSIGN : LEFT_SHIFT OP_ASSIGN;
RIGHT_SHIFT_ASSIGN : RIGHT_SHIFT OP_ASSIGN;
POWER_ASSIGN : POWER_OP OP_ASSIGN;
IDIV_ASSIGN : IDIV_OP OP_ASSIGN;


/* *** Literrals *** */
/* ** Numbers ** */
fragment SIGN: '+' | '-' ;
fragment DECIMAL_SEP
    : '.' /* Dot separator */
    | ',' /* Comma separator */
    | '⎖' /* U+2396 DECIMAL SEPARATOR KEY SYMBOL */
    | '٫' /* U+066B ARABIC DECIMAL SEPARATOR */
    ;

/* Base 10 default */
/* Limit to ASCII based arabic digits to represent base ≤10 numbers.
Support underscores for digit grouping */
fragment BASE_10: [0-9_]+ ;
fragment FLOAT_EXP_10: ('e' | 'E') BASE_10 ;
INT_10: SIGN? BASE_10 ;
FLOAT_10: SIGN? BASE_10 DECIMAL_SEP BASE_10 FLOAT_EXP_10? ;
COMPLEX_IMAGE_10: (INT_10 | FLOAT_10) 'i' ;

/* Other bases */
fragment RADIX: ';' BASE_10 ;  /* Base of the number is always written as a base 10 number */
fragment NON_10_INT: [0-9_A-Za-z]+ ;  /* Use ASCII alphabetic letters to extend range of digits beyond 10 */
fragment FLOAT_EXP: ('e' | 'E') NON_10_INT ;
INT_RADIX:  SIGN? NON_10_INT RADIX ;
FLOAT_RADIX: SIGN? NON_10_INT DECIMAL_SEP NON_10_INT FLOAT_EXP? RADIX ;
COMPLEX_IMAGE_RADIX: SIGN? (INT_RADIX | FLOAT_RADIX) 'i' RADIX ;

INFINITY: '∞';  /* U+221E INFINITY */

/* *** Strings *** */
STRING
    : '"' .*? '"'
    | '\'' .*? '\''
    ;


ID: [\p{XID_Start}][\p{XID_Continue}]* ;  /* http://www.unicode.org/reports/tr31/ */

LANG: ID ;  /* In theory, should conform to ISO 639 language code.
In practice, will match the filename of the localized keywords file */

PRE_KW: '`';  /* U+0060 GRAVE ACCENT (a.k.a. back-tick) */
KEYWORD: PRE_KW ID ;

SPACE: [\p{blank}]+ ;

EOL : '\r'? '\n' ;
